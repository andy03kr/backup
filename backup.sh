#!/usr/bin/env bash

# default simple backup
ACTION="0"

WHICH="/usr/bin/which"
ECHO=`$WHICH echo`

if [[ "$#" -eq "1" && "$1" =~ ^(image|rsync|full)$ ]]; then
  if [[ "$1" == "full" ]]; then
    ACTION="2"
  elif [[ "$1" == "image" ]]; then
    ACTION="1"
  fi
else
  $ECHO "USAGE: $0 full|half"
  $ECHO "where full  - create image backup and rsync backup"
  $ECHO "      image - create image backup only"
  $ECHO "      rsync - create rsync backup only"
  exit 1
fi

SSH=`$WHICH ssh`
SCP=`$WHICH scp`
SUDO=`$WHICH sudo`
FDISK=`$WHICH fdisk`
RSYNC=`$WHICH rsync`
DD=`$WHICH dd`
RM=`$WHICH rm`
BZIP=`$WHICH bzip2`
TEE=`$WHICH tee`
MKDIR=`$WHICH mkdir`
HOSTNAME=`$WHICH hostname`
HOSTNAME=`$HOSTNAME --fqdn`
DATE=`$WHICH date`
DATE=`$DATE +"%d%m%Y"`

BKPDISK="/dev/sda"

BKPUSR="backup"
BKPSRV="bkpsrv.home.lan"
BKPKEY="/home/backup/.ssh/id_rsa"
DISKSTATE="/tmp/diskgeometry.$DATE"
BKPPATH="/mnt/backup/$HOSTNAME"
RSYNCPATH="/mnt/backup/$HOSTNAME/rsync.$DATE"
SSHSTR="ssh -i $BKPKEY -o StrictHostKeyChecking=no"
EXCLUDE="--exclude dev/*     \ 
	 --exclude proc/*    \
	 --exclude sys/*     \
	 --exclude tmp/*     \
	 --exclude run/*     \
	 --exclude mnt/*     \
	 --exclude var/tmp/* \
	 --exclude media/*   \
	 --exclude cdrom/*   \
	 --exclude lost+found"

$ECHO start $(date +"%d.%m.%Y %H:%M:%S") > backup.log

$SSH -i $BKPKEY $BKPUSR@$BKPSRV "$MKDIR -p $RSYNCPATH" >> backup.log
if [[ "$?" -ne 0 ]]; then
  $ECHO "ERROR: cannot create backup directory $BKPPATH on $BKPSRV"
  exit 1
fi

if [[ "$ACTION" == "1" || "$ACTION" == "2" ]]; then
  $SUDO $FDISK -l > $DISKSTATE
  $SCP -i $BKPKEY $DISKSTATE $BKPUSR@$BKPSRV:$BKPPATH/ >> backup.log
  $SUDO $DD if=$BKPDISK | $BZIP -9 -c | $SSH -i $BKPKEY $BKPUSR@$BKPSRV "$DD of=$BKPPATH/$DATE.bzip2" >> backup.log
  $RM -f $DISKSTATE
fi

$ECHO finish image backup $(date +"%d.%m.%Y %H:%M:%S") >> backup.log

if [[ "$ACTION" == "0" || "$ACTION" == "2" ]]; then
  $SUDO $RSYNC -aAXPzve "$SSHSTR" $EXCLUDE / $BKPUSR@$BKPSRV:$RSYNCPATH/ >> backup.log
fi

$ECHO finish $(date +"%d.%m.%Y %H:%M:%S") >> backup.log

exit 0
